#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include <common.h>

struct Queue
{
    unsigned int qSize;
    node *head;
    node *tail;
};
typedef struct Queue*_Queue;

/**
 * @brief      Create Queue
 *
 * @return     On success returns a pointer to memory. On failure check errno. 
 */
_Queue			Queue();

/**
 * @brief      Push an object into queue.
 *
 * @param[in]  q          The queue pointer
 * @param[in]  object       The object pointer
 * @param[in]  freeFunc   An optional callback to freed memory when flushQueue is invoked. If no freed memory is needed for the object the set this parameter to NULL, but you are responsible for releasing object's memory.
 *
 * @return     On success 0, on error -1
 */
int				enqueue(_Queue q, void *object, void(freeFunc)(void*)=free);
/**
 * @brief      Take 
 *
 * @param[in]  q     The queue pointer.
 * 
 * @return     If queue is empty the function returns NULL. If queue has elements a pointer to the first element that entered the queue is returned.  
 */
void			*dequeue(_Queue q);

/**
 * @brief      Empties the queue. If a freeFunc was provided on 
 *
 * @param[in]  q     The quarter
 */
void			flush(_Queue q);
unsigned int	count(_Queue q);

void            destroy(_Queue q);

#endif /* QUEUE_H_INCLUDED */
