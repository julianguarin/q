#include <stdlib.h>
#include "queue.h"


void init(_Queue q)
{
	if(!q)return;
	q->qSize = 0;
	q->head = q->tail = NULL;
}
void destroy_Queue(_Queue q){
    flush(q);
    free(q);
}
_Queue Queue(){

	_Queue q = (_Queue) malloc(sizeof(struct Queue));
	init(q);
	return q;	
}	


int enqueue(_Queue q, void *data, void(freeFunc)(void*))
{	
	if(!data)return -1;
	node *newNode = (node *)malloc(sizeof(node));
	
    if(newNode == NULL) return -1;
    
    
    newNode->data = data;
	newNode->next = NULL;
	newNode->freeFunc = freeFunc;
	
    if(!q->head)q->head=q->tail=newNode;
	else q->tail=q->tail->next=newNode;
	q->qSize++;
	return 0;
}

void * dequeue(_Queue q)
{
    if (!q) return NULL;
    if(!q->head)return NULL;
	void * ret = q -> head -> data;
	node * toKill = q -> head;
	
	q -> head = q -> head -> next;
	q -> qSize--;
	q -> tail = q -> qSize ? q -> tail : NULL;
	
	free(toKill);

	return ret;

}


void flush(_Queue q)
{
    if (!q) return;
	  
    if (!q->qSize) return;
    node *temp;
    
    while(--q->qSize)
	{	
		if (q->head->freeFunc){
			q->head->freeFunc(q->head->data);
		}
		temp = q->head;
		q->head=q->head->next;
		free(temp);
	}
	q->head = q->tail = NULL;
}

unsigned int count(_Queue q)
{
    if (q)
        return q->qSize;
    return 0;
}
void destroy(_Queue q ){
    if (!q) return;
    flush (q);
    free(q);
    
}
